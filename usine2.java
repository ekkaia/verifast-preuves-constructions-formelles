//@predicate tache(Tache tache; int temps_necessaire, int gain) = tache.temps_necessaire |-> temps_necessaire &*& tache.gain |-> gain &*& temps_necessaire >= 0 &*& gain >= 0;
public class Tache {
	private int temps_necessaire;
	private int gain;

	public Tache(int temps_necessaire, int gain)
	//@requires temps_necessaire >= 0 &*& gain >= 0;
	//@ensures tache(this, temps_necessaire, gain);
	{
		this.temps_necessaire = temps_necessaire;
		this.gain = gain;
	}

	public int get_gain()
	//@requires tache(this, ?b, ?c);
	//@ensures tache(this, b, c) &*& result == c;
	{
		return this.gain;
	}

	public int get_temps_necessaire()
	//@requires tache(this, ?b, ?c);
	//@ensures tache(this, b, c) &*& result == b;
	{
		return this.temps_necessaire;

	}
}

//@predicate travailleur(Travailleur travailleur; int temps_dispo, int salaire_horaire, int salaire_percu) = travailleur.temps_dispo |-> temps_dispo &*& travailleur.salaire_horaire |-> salaire_horaire &*& travailleur.salaire_percu |-> salaire_percu &*& salaire_horaire >= 0 &*& temps_dispo >= 0;
public class Travailleur {
	private int temps_dispo;
	private int salaire_percu;
	private int salaire_horaire;

	public Travailleur(int temps_dispo, int salaire_horaire)
	//@requires temps_dispo >= 0 &*& salaire_horaire >= 0;
	//@ensures travailleur(this, temps_dispo, salaire_horaire, 0);
	{

		this.temps_dispo = temps_dispo;
		this.salaire_percu = 0;
		this.salaire_horaire = salaire_horaire;
	}

	public int get_temps_dispo()
	//@requires travailleur(this, ?a, ?b, ?c);
	//@ensures travailleur(this, a, b, c) &*& result == a;
	{
		return this.temps_dispo;
	}

	public int get_salaire_horaire()
	//@requires travailleur(this, ?a, ?b, ?c);
	//@ensures travailleur(this, a, b, c) &*& result == b;
	{
		return this.salaire_horaire;
	}

	public int get_salaire_percu()
	//@requires travailleur(this, ?a, ?b, ?c);
	//@ensures travailleur(this, a, b, c) &*& result == c;
	{
		return this.salaire_percu;
	}

	public int travailler(int t)
	//@requires travailleur(this, ?a, ?b, ?c) &*& t >= 0;
	//@ensures t <= a ? travailleur(this, a - t, b, c + b * t) : true &*& result == c;
	{
		if (t <= this.temps_dispo) {
			this.temps_dispo -= t;
			this.salaire_percu += t * this.salaire_horaire;
		}
		return this.salaire_percu;
	}
}

//@predicate usine(Usine usine; int depenses_salaire, int gains_tache) = usine.depenses_salaire |-> depenses_salaire &*& usine.gains_tache |-> gains_tache;
//@predicate estEngage(Usine usine, Travailleur travailleur) = true;
public class Usine {
	private int depenses_salaire;
	private int gains_tache;

	public Usine(int gains_initiaux, int depenses_initiales)
	//@requires gains_initiaux >= 0 &*& depenses_initiales <= 0;
	//@ensures usine(this, depenses_initiales, gains_initiaux);
	{
		this.gains_tache = gains_initiaux;
		this.depenses_salaire = depenses_initiales;
	}

	public void gains(int argent)
	//@requires usine(this, ?depenses_salaire, ?gains_tache);
	//@ensures usine(this, depenses_salaire , gains_tache + argent);
	{
		this.gains_tache += argent;
	}

	public void depenses(int argent) 
	//@requires usine(this, ?depenses_salaire, ?gains_tache);
	//@ensures usine(this, depenses_salaire - argent, gains_tache);
	{
		this.depenses_salaire -= argent;
	}

	public static boolean est_rentable(Tache tache, Travailleur travailleur)
	//@ requires tache(tache, ?temps_necessaire, ?gain) &*& travailleur(travailleur, ?temps_dispo, ?salaire_horaire, ?salaire_percu) &*& tache != null &*& travailleur != null;
	//@ ensures tache(tache, temps_necessaire, gain) &*& travailleur(travailleur, temps_dispo, salaire_horaire, salaire_percu) &*& result == (gain > salaire_horaire * temps_necessaire);
	{
		return tache.get_gain() > travailleur.get_salaire_horaire() * tache.get_temps_necessaire();
	}

	public void effectue_tache(Tache tache, Travailleur travailleur)
	//@ requires usine(this, ?depenses_salaire, ?gains_tache) &*& tache(tache, ?temps_necessaire, ?gain) &*& travailleur(travailleur, ?temps_dispo, ?salaire_horaire, ?salaire_percu) &*& tache != null &*& travailleur != null &*& estEngage(this, travailleur) &*& temps_dispo > 0;
	//@ ensures (temps_dispo >= temps_necessaire && gain > salaire_horaire * temps_necessaire) ? travailleur(travailleur, temps_dispo - temps_necessaire, salaire_horaire, salaire_percu + salaire_horaire * temps_necessaire) &*& usine(this, depenses_salaire - temps_necessaire * salaire_horaire, gains_tache + gain) : true;
	{
		if (travailleur.get_temps_dispo() >= tache.get_temps_necessaire() && est_rentable(tache, travailleur)) {
			//@ open tache(tache, _, _);
			//@ open travailleur(travailleur,  _, _, _);
			this.depenses(tache.get_temps_necessaire() * travailleur.get_salaire_horaire());
			travailleur.travailler(tache.get_temps_necessaire());
			this.gains(tache.get_gain());
		}
	}

	public int get_balance()
	//@requires usine(this, ?depenses_salaire, ?gains_tache);
	//@ensures usine(this, depenses_salaire, gains_tache) &*& result == gains_tache - depenses_salaire;
	{
		return this.gains_tache - this.depenses_salaire;
	}

	public void engager(Travailleur travailleur)
	//@ requires travailleur(travailleur ,?temps_dispo,?salaire_horaire ,? salaire_percu);
	//@ ensures travailleur(travailleur ,temps_dispo,salaire_horaire,salaire_percu) &*& estEngage(this, travailleur);
	{
	//@close estEngage(this , travailleur );
	}

	public void renvoyer(Travailleur travailleur) 
	//@ requires travailleur(travailleur, ?temps_dispo, ?salaire_horaire, ?salaire_percu) &*& estEngage(this, travailleur);
	//@ ensures travailleur(travailleur, temps_dispo, salaire_horaire, salaire_percu);
	{
	//@open estEngage(this, travailleur);
	}
}

public class UsineTest {
	public static void main(String[] args)
	//@requires true;
	//@ensures true;
	{

		/**
		 * À tester 
		 * 
		 * 	Tache : 
		 * 		Test methode accesseur
		 * 		- get_gain()
		 * 		- get_temps_necessaire()
		 * 
		 * 	Travailleur : 
		 * 		Test methode accesseur
		 * 		- get_temps_dispo()
		 * 		- get_salaire_horaire()
		 * 		- get_salaire_percu()
		 * 		Test si temps_dispo -= t &*& salaire_percu += t * salaire_horaire;
		 * 		- travailler(int t)
		 * 
		 * 	Usine : 
		 * 		Test si gains_tache += argent;
		 * 		- gains()
		 * 		Test si depenses_salaire -= argent; 
		 * 		- depenses()
		 *		Test methode accesseur
		 * 		- get_balance()
		 * 		Test si tache.get_gain() > travailleur.get_salaire_horaire() * tache.get_temps_necessaire();
		 * 		- est_rentable(Tache tache, Travailleur travailleur)
		 * 		Test si est_rentable renvoie vraie et travailleur.get_temps_dispo() >= tache.get_temps_necessaire()
		 * 		- effectue_tache(Tache tache, Travailleur travailleur)
		 * 		Test si effectue_tache s'execute bien après avoir engager le travailleur 
		 * 		et ne s'execute pas après avoir renvoyer le trvailleur
		 * 		- engager(Travailleur)
		 * 		- renvoyer(Travailleur)
		 */

		int temps_necessaire = 4;
		int gain             = 10;
		int gain_tache       = 6; 
		int depenses_salaire = -2; 
		int temps_dispo      = 10;
		int temps_dispo2     = 0; 
		int salaire_horaire  = 2;

		Tache tache             = new Tache(temps_necessaire, gain);
		Usine usine             = new Usine(gain_tache, depenses_salaire);
		Travailleur travailleur = new Travailleur(temps_dispo, salaire_horaire);
		Travailleur travailleur2 = new Travailleur(temps_dispo2, salaire_horaire);

		//Tache
			int gainOfTask    = tache.get_gain(); 
			int necessaryTime = tache.get_temps_necessaire(); 
			assert gainOfTask    == 10; 
			assert necessaryTime == 4; 

		//Travailleur
			int availableTime  = travailleur.get_temps_dispo(); 
			int hourlyEarnings = travailleur.get_salaire_horaire();   
			int salaryEarned   = travailleur.get_salaire_percu(); 
			assert availableTime  == 10; 
			assert hourlyEarnings == 2; 
			assert salaryEarned   == 0; 

			int salaryEarnedBefore    = travailleur.get_salaire_percu(); 
			travailleur.travailler(2);
			
			hourlyEarnings = travailleur.get_salaire_horaire();
			salaryEarnedBefore = salaryEarnedBefore + (2 * hourlyEarnings); 
			int salaryEarnedAfterWork1 = travailleur.get_salaire_percu(); 
			assert salaryEarnedBefore == 4;
			assert salaryEarnedAfterWork1 == 4; 

		//Usine
			int balance = gain_tache - depenses_salaire; 
			int trueBalance = usine.get_balance(); 
			assert balance == trueBalance; 
				
			usine.depenses(1);
			usine.gains(2);
			balance = usine.get_balance(); 
			assert balance == 11; 

			usine.gains(10);
			balance = usine.get_balance(); 
			assert balance == 21; 

			boolean profitableIsTrue = usine.est_rentable(tache, travailleur); 
			assert profitableIsTrue == true; 

			usine.engager(travailleur); 
			//Enlever commentaire pour la question 9 et montrer que cela lève une exception
			//usine.renvoyer(travailleur); 
			usine.effectue_tache(tache, travailleur);
			salaryEarned = travailleur.get_salaire_percu(); 
			balance      = usine.get_balance();
			assert salaryEarned == 12; 
			assert balance      == 39;
			
			//Enlever commentaire pour la question 7 et montrer que ca lève une exception pour impossibilité d'effectuer deux fois la même tache
			//usine.engager(travailleur); 
			//usine.effectue_tache(tache, travailleur);
			
			usine.engager(travailleur2); 
			//Enlever commentaire pour la question 10 et montrer que cela lève une exception car temps_dispo <= 0
			//usine.effectue_tache(tache, travailleur2);
	}
}
