# Liste des choses à mettre dans le rapport

3. Expliquer brièvement dans le rapport les modifications à apporter à la définition du prédicat
travailleur et au code de la classe T ravailleur qu’il faudrait effectuer (Il n’est pas demandé
d’intégrer ces modifications au code source que vous fournirez).

6. On remplace l'attribut `balance` par `depenses_salaire` (négatif ou nul) et `gains_tache` (positif ou nul). On remplace la méthode `deposer_argent(int argent)` par `depenses(int argent)` et `gains(int argent)` pour gérer les dépenses de salaire et les gains des tâches. Dans la méthode `effectue_tache(Tache tache, Travailleur travailleur)`, on remplace l'appel à `deposer_argent(int argent)` par les méthodes précedemment citées. Le getter `get_balance()` retourne la soustraction entre les gains des tâches et les dépenses de salaire. [WIP PARTIE SUR USINE TEST]

8. On crée un prédicat engager, tant qu'il n'a pas été généré dans engager, il ne peut pas être consommer dans effectuer tache.
    - Expliquer dans le rapport comment fonctionne cette spécification, et s’il est possible de contourner cette garantie.
    - Rajouter dans la fonction main de T estUsine des instructions pour tester ces fonctions.

9. 
    - Rajouter dans la fonction main de T estUsine des instructions pour tester ces fonctions.
