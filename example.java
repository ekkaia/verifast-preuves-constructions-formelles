//@predicate compte(Compte a; int b) = a.transactions |-> ?t &*& transactions(t, b) &*& t != null ;
class Compte 
{
	private Transactions transactions;
	
	public Compte()
	//@ requires true;
	//@ ensures compte(this, 0);
	{
		this.transactions = new Transactions(0, null);
	}
	
	public int obtenirBalance()
	//@ requires compte(this, ?b);
	//@ ensures compte(this, b) &*& result == b;
	{
		return this.transactions.obtenirTotal();
	}
	
	public void depot (int montant)
	//@ requires compte(this, ?b) &*& montant >= 0;
	//@ ensures compte(this, b + montant);
	{
		this.transactions = new Transactions(montant, this.transactions);
	}
	
	public void retrait (int montant)
	//@ requires compte(this, ?b) &*& montant >= 0;
	//@ ensures b >= montant ? compte(this, b - montant) : true;
	{
		if (this.obtenirBalance() >= montant) {
			this.transactions = new Transactions(-montant, this.transactions);
		}
	}
	
	public Compte clone()
	//@ requires compte(this, ?b) &*& b >= 0;
	//@ ensures compte(result, b) &*& compte(this, b);
	{
		Compte nouveauCompte = new Compte();
		nouveauCompte.depot(this.obtenirBalance());
		
		return nouveauCompte;
	}
	
	public void transfert (Compte autre, int montant)
	//@ requires compte(this, ?b1) &*& compte(autre, ?b2) &*& montant >= 0;
	//@ ensures b1 >= montant ? compte(this, b1 - montant) &*& compte(autre, b2 + montant) : true ;
	{
		if (this.obtenirBalance() >= montant ) {
			//@open compte(autre, _);
			autre.depot(montant);
			this.retrait(montant);
		}
	}		
}
/*@
predicate transactions(Transactions t; int total) = 
		t == null ? total == 0 : t.montant |-> ?montant &*& t.suivant |-> ?suivant
		&*& transactions(suivant, ?ntotal) &*& total == montant + ntotal;
@*/
class Transactions {
	private int montant;
	private Transactions suivant;
	
	public Transactions(int montant, Transactions suivant)
	//@requires transactions(suivant, ?total);
	//@ensures transactions(this, montant+total);
	{
		this.montant = montant; this.suivant = suivant;
	}
	
	public int obtenirTotal()
	//@requires transactions(this, ?total);
	//@ensures transactions(this, total) &*& result == total;
	{
		if (this.suivant == null) {
			//@open transactions(this.suivant, _);
			return this.montant;
		} else {
			return this.montant + this.suivant.obtenirTotal();
		}
	}
}
		

class CompteClient {
	public static void main(String[] args)
	//@ requires true;
	//@ ensures true;
	{
		Compte a = new Compte();
		a.depot(100);
		Compte b = a.clone();
		//@open compte(b, _);
		b.depot(50);
		int tmp = a.obtenirBalance();
		assert tmp == 100;
		a.transfert(b, 20);
		tmp = b.obtenirBalance();
		assert tmp == 170;
	}
}
